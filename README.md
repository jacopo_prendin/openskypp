
# Task For Primephonic
The assignment was to load and visualize *all* data from OpenSky, paying attention to give a smooth experience to the user.\
The class *Flight* contains all basic informations of a flight PLUS a history of all data loaded from OpenSky since application started; these entries are stored on a *FlightDataEntry*.\
Alamofire grants an asynchronous update of all datas and SwiftyJSON a simple way to parse datas from OpenSky.\
Once the application is closed, datas are lost; anyway, *Flight* and *FlightDataEntry* could be easily stored on disk with SQLite.\
I put the AccessibilityIndetifiers on all used widgets. This is useful for accessibility, but also for UI Testing, my task at my current workplace.
