//
//  Flight.swift
//  PrimeAssign
//
//  Created by Jacopo Prendin on 10/04/2019.
//  Copyright © 2019 jprendin. All rights reserved.
//

import Foundation
import MapKit

class PPAnnotation:MKPointAnnotation{
    var icao24 = "undefined"
}
/*------------------------------------------------------------------------*/
// MARK: -

/**
 * All Flight informations that could change during time are stored on instances
 * of this class
 */
class FlightDataEntry {
    var time_position:Int?
    var last_contact:Int
    var last_update:Int // this is our datetime
    var longitude:Double?
    var latitude:Double?
    var baro_altitude:Double?
    var on_ground:Bool
    var velocity:Double?
    var true_track:Double?
    var vertical_rate:Double?
    var sensors:[Int]?
    var geo_altitude:Double?
    var squawk:String?
    var spi:Bool
    var position_source:Int
    
    /**
     * The init is the same used by Flight class.
     */
    init( _ rest_array:[Any?]) {
        self.time_position = rest_array[3] as? Int
        self.last_contact = rest_array[4] as! Int
        self.longitude = rest_array[5] as? Double
        self.latitude = rest_array[6] as? Double
        self.baro_altitude = rest_array[7] as? Double
        self.on_ground = rest_array[8] as! Bool
        self.velocity = rest_array[9] as? Double
        self.true_track = rest_array[10] as? Double
        self.vertical_rate = rest_array[11] as? Double
        self.sensors = rest_array[12] as? [Int]
        self.geo_altitude = rest_array[13] as? Double
        self.squawk = rest_array[14] as? String
        self.spi = rest_array[15] as! Bool
        self.position_source = rest_array[16] as! Int
        
        self.last_update = Int(Date().timeIntervalSince1970)
    }
}

/*------------------------------------------------------------------------*/
// MARK: -
class Flight {
    // for fields informations, see https://opensky-network.org/apidoc/rest.html
    var icao24:String  /**< icao24 is used as identifier for the flight */
    var callsign:String?
    var origin_country:String
    var history:[FlightDataEntry]
    var annotation:PPAnnotation
    
    /* Properties introduced to get easy access to latest "interesting" infos */
    var last_latitude:Double? {
        get {
            if self.history.last == nil {
                return 0.0
            } else {
                return self.history.last?.latitude
            }
        }
    }
    
    var last_longitude:Double? {
        get {
            if self.history.last == nil {
                return 0.0
            } else {
                return self.history.last?.longitude
            }
        }
    }
    
    var last_contact:Int? {
        get {
            if self.history.last == nil { return nil }
            else { return self.history.last?.last_contact}
        }
    }
    
    var last_update:Int? {
        get {
            if self.history.last == nil { return nil }
            else { return self.history.last?.last_update}
        }
    }
    
    /**
     * Updates annotation according to latest saved data
     */
    func setAnnotation(){
        self.annotation.title = self.icao24
        self.annotation.icao24 = self.icao24
        guard let latest_infos = self.history.last else {
            self.annotation.coordinate = CLLocationCoordinate2DMake(0.0, 0.0)
            return
        }
        
        if latest_infos.latitude != nil && latest_infos.longitude != nil {
            self.annotation.coordinate = CLLocationCoordinate2DMake(
                latest_infos.latitude!,
                latest_infos.longitude!)
        }
    }
    
    /**
     * Init
     */
    init(_ rest_array:[Any?]){
        self.icao24 = rest_array[0] as! String
        self.callsign = rest_array[1] as? String
        self.origin_country = rest_array[2] as! String
        self.history = []
        self.annotation = PPAnnotation()
        
        self.history.append(FlightDataEntry(rest_array))
        self.setAnnotation()
    }
    
    /**
     * Adds an entry and update the annotation informations
     */
    func addEntry(_ new_entry:FlightDataEntry){
        self.history.append(new_entry)
        self.setAnnotation()
    }
}
