//
//  ViewController.swift
//  PrimeAssign
//
//  Created by Jacopo Prendin on 05/04/2019.
//  Copyright © 2019 jprendin. All rights reserved.
//

import UIKit
import MapKit

import Alamofire
import SwiftyJSON

let OPENSKY_API:String = "https://opensky-network.org/api/states/all"

class ViewController: UIViewController,MKMapViewDelegate, CLLocationManagerDelegate {
    
    /* all the widgets used on this view */
    @IBOutlet weak var radar_map: MKMapView!
    @IBOutlet weak var first_download_indicator:UIActivityIndicatorView!
    @IBOutlet weak var label_icao24: UILabel!
    @IBOutlet weak var label_longitude: UILabel!
    @IBOutlet weak var label_latitude: UILabel!
    @IBOutlet weak var infos_view: UIView!
    
    
    let decoder = JSONDecoder()
    var flights:[String:Flight] = [:]
    var choosen_icao24 = ""

    let locationManager = CLLocationManager()
    /**
     * Here we get asynchronously data from OpenSky and we save these
     * informations in the flights Map.
     */
    @objc func refreshFlights() {
        Alamofire.request(OPENSKY_API).responseJSON { response in
            print("Result: \(response.result)")
            self.first_download_indicator.stopAnimating()
            self.first_download_indicator.isHidden = true
            if let json_data = response.data {
                do {
                    let list_flights = try JSON(data: json_data)
                    /*
                     * If we have no entries for this icao24, we create a new
                     * Flight object. Otherwise, we append these new datas to
                     * flight's history
                     */
                    for entry in list_flights["states"] {
                        let jsonarray = entry.1.arrayObject!
                        let flight_icao24 = jsonarray[0] as! String
                        
                        if self.flights[flight_icao24] == nil {
                            self.flights[flight_icao24] = Flight(jsonarray)
                        }
                        
                        let new_flight_entry = FlightDataEntry(jsonarray)
                        self.flights[flight_icao24]?.addEntry(new_flight_entry)
                    }
                    
                    // flights informations up-to-date. Show them
                    self.updateMap(self.radar_map)
                } catch let error {
                    print("Unable to parse: \(error)")
                }
            }
            else {
                print("No")
            }
        }
    }
    
    /**
     * viewDidLoad
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        self.first_download_indicator.startAnimating()
        self.infos_view.isHidden = true
        
        // start to get user's position
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        // show the whole world
        let span = MKCoordinateSpan(latitudeDelta: 180, longitudeDelta: 360)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2DMake(0.0, 0.0), span: span);
        self.radar_map.setRegion(region, animated: false)
        
        // get a first update
        self.refreshFlights()
        self.updateMap(self.radar_map)
        
        // schedule updates every 10 seconds
        Timer.scheduledTimer(timeInterval:10,
            target: self,
            selector: #selector(self.refreshFlights),
            userInfo: nil,
            repeats: true)
        }
    
    /**
     * When called, this method analise the give mapview region and provide
     * to redraw all necessary markers
     */
    func updateMap(_ mapView:MKMapView) {
        for (_,flight) in self.flights {
            if flight.last_latitude != nil && flight.last_longitude != nil {
                radar_map.addAnnotation(flight.annotation as MKAnnotation)
            }
        }
    }
    
    /**
     * When user touch an annotation, brief summary about that flight is
     * visualized
     */
    func mapView(_ mapView: MKMapView,didSelect view: MKAnnotationView) {
        let annotation = view.annotation as! PPAnnotation
        self.choosen_icao24 = annotation.icao24
        self.label_icao24.text = annotation.icao24
        self.label_latitude.text = "\(annotation.coordinate.latitude)"
        self.label_longitude.text = "\(annotation.coordinate.longitude)"
        self.infos_view.isHidden = false
    }
    
    /**
     * Pass flight informations to the DetailsView
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is DetailsViewController
        {
            guard let dvc = segue.destination as? DetailsViewController else {
                return
            }
            
            dvc.flight = self.flights[self.choosen_icao24]
        }
    }
    
    /**
     * Center map on user location
     */
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            print("\(location)")
            radar_map.setCenter(location.coordinate, animated: true)
        }
    }
    
    /**
     * Closes the info view
     */
    @IBAction func hideInfoView(){
        self.infos_view.isHidden = true
    }
}
