//
//  DetailsViewController.swift
//  PrimeAssign
//
//  Created by Jacopo Prendin on 10/04/2019.
//  Copyright © 2019 jprendin. All rights reserved.
//

import UIKit

class CellFlightEntry: UITableViewCell {
    
    @IBOutlet weak var label_time: UILabel!
    @IBOutlet weak var label_latitude: UILabel!
    @IBOutlet weak var label_longitude: UILabel!
    @IBOutlet weak var label_last_contact: UILabel!
}

class DetailsViewController: UITableViewController {
    @IBOutlet weak var table_history:UITableView!

    var flight:Flight? = nil
   
    let date_formatter = DateFormatter()
    
    /**
     * viewDidLoad
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        date_formatter.dateFormat = "yyyy-MM-d, HH:mm:SS"
        table_history.reloadData()
        
        // schedule updates every 10 seconds
        Timer.scheduledTimer(timeInterval:10,
                             target: self,
                             selector: #selector(self.reloadHistory),
                             userInfo: nil,
                             repeats: true)
    }
    
    @objc func reloadHistory() {
        self.table_history.reloadData()
    }
    /**
     * set datas on cell
     */
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "flight_entry_cell", for: indexPath)
            as! CellFlightEntry

        if flight != nil{
            let entry = flight!.history[indexPath.row]
            
            /* Converts the UNIX Timestamp to a String */
            let last_update = Date(timeIntervalSince1970: TimeInterval(exactly: entry.last_update)!)
            let last_conctact = Date(timeIntervalSince1970: TimeInterval(exactly: entry.last_contact)!)
            
            /* set labels with time informations */
            cell.label_time?.text = "Position at " + self.date_formatter.string(from: last_update)
            cell.label_last_contact?.text = "Last Contact: "+self.date_formatter.string(from: last_conctact)
            
            /* print coordinates */
            let lat:Double? = entry.latitude
            let lon:Double? = entry.longitude
            
            cell.label_latitude?.text = ( lat != nil ? "\(lat!)" : "N.A.")
            cell.label_longitude?.text =  (lon != nil ? "\(lon!)" : "N.A.")
            
            /* set accessibility identifiers (for future UI Testing)*/
            cell.label_time.accessibilityIdentifier = "time_for_\(indexPath.row)"
            cell.label_latitude.accessibilityIdentifier = "lat_for_\(indexPath.row)"
            cell.label_longitude.accessibilityIdentifier = "lon_for_\(indexPath.row)"
            cell.label_last_contact.accessibilityIdentifier = "last_contact_for_\(indexPath.row)"

        }
        return cell
    }
    
    
    /*------------------------------------------------------------------------*/
    // MARK: - Table Layout Overrides
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Flight Callsign: " +
            (flight!.callsign != nil ? (flight!.callsign!) : "N.A.")
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if flight != nil {
            return flight!.history.count
        }
        return 0
    }
}
